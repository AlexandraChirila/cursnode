var express = require('express');
var app = express();
var sql = require('mssql');
//var handlebars = require('express-handlebars');
var port = process.env.PORT || 3003;
var nav =  [{
    Link: '/Books',
    Text: 'Book'
},
    {
        Link: '/Authors',
        Text: 'Author'
    }];

var bookRouter = require('./src/routes/bookRoutes')(nav);
var adminRouter = require('./src/routes/admRoutes')(nav);

app.use(express.static('public'));
app.set('views', './src/views');
app.set('view engine', 'ejs');


app.use('/Books', bookRouter);
app.use('/Admin', adminRouter);

app.get('/', function (req, res) {
    res.render('index', {
        title: 'Hello ejs lovers? do you like how it renders?',
        nav: [{
            Link: '/Books',
            Text: 'Books'
        },
            {
                Link: '/Authors',
                Text: 'Authors'
            }]
    });
});

app.get('/books', function (req, res) {
    res.send('hello books');
});

app.listen(port, function (err) {
    console.log('server running on port ' + port);
});
